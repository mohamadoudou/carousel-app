import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import './index.css'
const contentArray = ['../media/image1.jpg', '../media/image2.jpg', '../media/image3.jpg', '../media/image5.jpg', '../media/video1.mp4', 'hello work', '../media/image5.jpg']
const ContentArrayLength = contentArray.length

function Header() {
    return (
        <div className='header__container'>
            <h3 className='header__title'>Carousel</h3>
        </div>
    )
}



function Card({ translateValue, content }) {
    return (
        <div className='card__container' style={{
            transform: `translateX(${-translateValue}%)`,
            marginLeft: content === '1' ? 0 : 0
        }}>
            {(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i).test(content) ?
                <img src={content} className="image" /> :
                (/\.(mp4|webm|wmv|avi|ogg|3gp)$/i).test(content) ?
                    <video controls width="100%" height="100%" className="video">
                        <source src={content}
                            type="video/webm" />

                        <source src={content}
                            type="video/mp4" />
                        Sorry, your videos extention is not supported by us.
                    </video> :
                    <p className='header__title text'>Not an Image {content}</p>
            }
        </div>
    )
}



function Slider() {
    const [translateValue, setTranslateValue] = useState(0)
    const [moveTo, setMoveTo] = useState(0)
    const [currentCardIndex, setCurrentCardIndex] = useState(0)
    const [position, setPosition] = useState(0)

    const handleTouchStart = (e) => {
        setMoveTo(e.nativeEvent.touches[0].clientX)
    }

    const handleTouchMove = (e) => {

        setPosition(moveTo - e.nativeEvent.touches[0].clientX)
        setMoveTo(e.nativeEvent.touches[0].clientX)
        handleMove()
    }
    const handleTouchEnd = (e) => {
        setMoveTo(0)
        handleMoveEnd()
    }

    const handleMove = () => {

        if (translateValue > (ContentArrayLength - 1) * 100) {

                setTranslateValue((ContentArrayLength - 1) * 100)
        }
        else if (translateValue < 0) {

            setTranslateValue(0)
        }
        else {
            setTranslateValue(translateValue + position)
        }
    }

    const handleMoveEnd = () => {
        const controlMove = (position + translateValue) % 100

        const endPosition = translateValue / 100
        //const endPosition = Math.trunc(translateValue / 100)
        const endPartial = endPosition % 1
        // const endPartial = (translateValue % 100)/100
        const endingIndex = endPosition - endPartial
        const deltaInteger = endingIndex - currentCardIndex
        let nextCardIndex = endingIndex
        if (deltaInteger >= 0) {
            if (endPartial >= 0.4) {
                nextCardIndex += 1
            }
        }
        else if (deltaInteger < 0) {
            nextCardIndex = currentCardIndex - Math.abs(deltaInteger);
            if (endPartial > 0.4) {
              nextCardIndex += 1;
            }
        }

        movementEnd(nextCardIndex)
    }

    const movementEnd = (cardIndex) => {
        if (currentCardIndex === 0) {
            setTranslateValue(cardIndex * 95)
            setCurrentCardIndex(cardIndex)
        }
        else {
            setTranslateValue(cardIndex * 100 - 5)
            setCurrentCardIndex(cardIndex)
        }
    }

    const handleNext = () => {
        if (translateValue < (ContentArrayLength - 1) * 100 - 5) {
            if (currentCardIndex === 0) {
                setTranslateValue(translateValue + 95)
                setCurrentCardIndex(currentCardIndex + 1)
            }
            else {
                setTranslateValue(translateValue + 100)
                setCurrentCardIndex(currentCardIndex+1)
            }
        }
    }
    const handlePrev = () => {
        if (translateValue > 0) {
            setTranslateValue(translateValue - 100)
            setCurrentCardIndex(currentCardIndex-1)
        }
        else{
            setTranslateValue(0)
        }
    }

    const handleGoto = (index) => {
        setCurrentCardIndex(index)
        if (currentCardIndex > 0) {
            movementEnd(index)
        }

    }

    return (
        <>
            <div
                className='slider__container'
                onTouchStart={handleTouchStart}
                onTouchMove={handleTouchMove}
                onTouchEnd={handleTouchEnd}
            >
                <span onClick={handlePrev} className="arrow arrow__left" ></span>
                <span onClick={handleNext} className="arrow arrow__right"></span>

                {contentArray.map((content, index) => {
                    return (
                        <Card key={index} translateValue={translateValue} content={content} />
                    )
                })}
            </div>
            <div className='button__container'>
                {
                    contentArray.map((content, index) => {
                        return (
                            <button
                                key={index}
                                onClick={e => handleGoto(index)}
                                className="button__shape"
                                style={{backgroundColor:currentCardIndex===index?'green':''}}
                            >
                                {index + 1}
                            </button>
                        )
                    })
                }
            </div>
        </>
    )
}



function Carousel() {
    return (
        <>
            <Header />
            <Slider />
        </>
    )
}



ReactDOM.render(
    <Carousel />,
    document.getElementById("root")
)